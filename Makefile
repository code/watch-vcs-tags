.POSIX:
.SUFFIXES:
.SUFFIXES: .sh
.PHONY: all install install-bin install-man clean
PREFIX = /usr/local
ALL = watch-git-tags
SH = /bin/sh
.sh:
	{ printf '#!%s\n\n' $(SH) ; cat $< ; } > $@
	chmod +x ./$@
all: $(ALL)
install: install-bin install-man
install-bin:
	mkdir -p -- $(PREFIX)/bin
	cp -- watch-git-tags $(PREFIX)/bin
install-man:
	mkdir -p -- $(PREFIX)/share/man/man1
	cp -- watch-git-tags.1 $(PREFIX)/share/man/man1
clean:
	rm -f -- $(ALL)
