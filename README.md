`watch-vcs-tags`
================

Scripts that given a list of repository roots as arguments look for new tags
available on remote systems, and report the repository name and a tab-indented
list of new tags, if any.

Presently only `watch-git-tags` is implemented (well, published). This is all
implemented in POSIX `sh`, but does require a `mktemp(1)` implementation that
supports the `-d` option to create temporary directories.

    $ watch-git-tags ~/.local/src/ed/vim
    /home/tom/.local/src/ed/vim
    	v8.0.0858

You can install it in `/usr/local/bin` with:

    # make install

You can define a `PREFIX` to install it elsewhere:

    $ make install PREFIX="$HOME"/.local

If you keep a list of your repository paths in a file like `~/.watch-git-tags`,
you could put this in a `crontab(5)` task to email you new tags:

    xargs watch-git-tags < ~/.watch-git-tags-repos

Another option is to watch all repositories in a specific directory:

    watch-git-tags "$HOME"/.local/src/watch/*

License
-------

Copyright (c) [Tom Ryder][1]. Distributed under an [MIT License][2].

[1]: https://sanctum.geek.nz/
[2]: https://www.opensource.org/licenses/MIT
